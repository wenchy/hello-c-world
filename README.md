# Hello-C-World: The development procedure of C Program #


## Introduction

- Project Homepage: <http://www.tinylab.org/project/hello-c-world/>
- Project Repository: [https://gitlab.com/tinylab/hello-c-world.git](https://gitlab.com/tinylab/hello-c-world/)

Please check doc/BUILD.md for building and doc/README.md for the project details.

## Errata

If you see anything that is technically wrong or otherwise in need of
correction, please email me at wuzhangjin at gmail dot com to inform me.

### License

The license is under ![](http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png), see [CC BY NC ND 3.0](http://creativecommons.org/licenses/by-nc-nd/3.0/) for more
